#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>

#include <linux/moduleparam.h>
#include <linux/printk.h>

#include <linux/atomic.h>
#include <linux/semaphore.h>

#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/cdev.h>

#include <linux/kprobes.h>
#include <linux/fs_struct.h>

#include <linux/time.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("lch361");
MODULE_DESCRIPTION("Registers and logs file creation, deletion and modification.");
MODULE_VERSION("1.0.0-x86_64");

typedef struct semaphore semaphore;
typedef struct file_operations file_operations;
typedef struct class class;
typedef struct kretprobe kretprobe;
typedef struct kretprobe_instance kretprobe_instance;
typedef struct pt_regs pt_regs;

#define MODNAME "fcdlogger"
#define CHARDEV_NAME "fcdl"
#define SUCCESS 0
#define FAIL    1

#define CREATION	 0b100
#define DELETION 	 0b10
#define MODIFICATION 0b1

static uint actions = 0b110;
module_param(actions, uint, 0000);
MODULE_PARM_DESC(actions,
		"Actions which will be logged in: "
		"2nd bit is for file creation, "
		"1st bit is for file deletion, "
		"0th bit is for file modification");

static bool record_time = true;
module_param(record_time, bool, 0000);
MODULE_PARM_DESC(record_time, "Write the time in %HH:%MM:%SS");

// TODO: write also current date of event
static char buffer[PATH_MAX + 2]; // +2 is for additional info
static char time_buffer[9];
static atomic_t file_available   = ATOMIC_INIT(1);
static atomic_t buffer_available = ATOMIC_INIT(0);
static semaphore sem_can_read_buffer;

static int chardev_major;
static class *cls;

static kretprobe create_kretprobe = {}, remove_kretprobe = {};

static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char __user *, size_t, loff_t *);

static file_operations device_ops = {
    .read    = device_read,
    .open    = device_open,
    .release = device_release,
};

static int device_open(struct inode *_inode, struct file *_file)
{
	// We allow only one reader at time
	if (atomic_cmpxchg(&file_available, 1, 0) == 0)
	{
		return -EBUSY;
	}
	try_module_get(THIS_MODULE);
	return SUCCESS;
}

static int device_release(struct inode *_inode, struct file *_file)
{
	atomic_set(&file_available, 1);
	atomic_set(&buffer_available, 0);
	// You must never remove that line again,
	// or else the world will be destroyed!
	module_put(THIS_MODULE);
	return SUCCESS;
}

static ssize_t device_read(struct file *_file, char __user *user_buf,
		size_t user_buf_length, loff_t *offset)
{
	ssize_t buffer_chars_written = 0;
	char *i = buffer, *j;

	if (user_buf_length < sizeof(buffer))
	{
		return 0;
	}

	// Unlock buffer
	atomic_set(&buffer_available, 1);

	// Waiting when the event occurs which writes to buffer
	sema_init(&sem_can_read_buffer, 0);
	if (down_interruptible(&sem_can_read_buffer) == -EINTR)
	{
		// But if the user interrupted, stop
		atomic_set(&buffer_available, 0);
		return 0;
	}

	// Buffer is already locked by the same process
	// which added the semaphore
	
	// Write time if enabled
	if (record_time)
	{
		for (j = time_buffer; j != time_buffer + 8; ++j)
		{
			put_user(*j, user_buf++);
		}
		put_user(' ', user_buf++);
		buffer_chars_written += 9;
	}

	// Write the rest of the path
	for (; *i != '\0'; ++i)
	{
		put_user(*i, user_buf++);
	}
	put_user('\n', user_buf++);
	buffer_chars_written += i - buffer + 1;

	user_buf_length      -= buffer_chars_written;
	*offset              += buffer_chars_written;

	return buffer_chars_written;
}

static int forward_registers(kretprobe_instance *krp, pt_regs *kern_regs)
{
	pt_regs **krp_shared_regs = (pt_regs **)krp->data;
	*krp_shared_regs = (pt_regs *)kern_regs->di;
	return 0;
}

static int on_openat(kretprobe_instance *krp, pt_regs *kern_regs)
{
	int return_code = kern_regs->ax;
	pt_regs *regs = *(pt_regs **)krp->data;
	int flags = regs->dx;
	int dfd   = regs->di;
	const char __user *filename = (const char __user *)regs->si;
	char current_ch;
	int16_t i = 2;
	
	if (return_code < 0 || (flags & O_CREAT) == 0
			|| atomic_cmpxchg(&buffer_available, 1, 0) == 0)
	{
		return 0;
	}

	// get time
	if (record_time)
	{
		time64_t current_time = ktime_get_real_seconds() % (3600 * 24);
		int hours   = current_time / 3600;
		int minutes = current_time % 3600 / 60;
		int seconds = current_time % 3600 % 60;

		sprintf(time_buffer, "%.2i:%.2i:%.2i", hours, minutes, seconds);
	}

	buffer[0] = '+';
	buffer[1] = ' ';

	get_user(current_ch, filename++);

	if ((dfd & AT_FDCWD) != 0 && current_ch != '/')
	{
		char *cwd = d_path(&current->fs->pwd, buffer, sizeof(buffer));
		for (; *cwd != '\0'; ++cwd)
		{
			buffer[i++] = *cwd;
		}
		if (buffer[i - 1] != '/')
		{
			buffer[i++] = '/';
		}
	}
	buffer[i++] = current_ch;

	for (get_user(current_ch, filename++);
			current_ch != '\0';
			get_user(current_ch, filename++))
	{
		buffer[i++] = current_ch;
	}

	buffer[i++] = '\0';
	up(&sem_can_read_buffer);
	return 0;
}

static int on_unlinkat(kretprobe_instance *krp, pt_regs *kern_regs)
{
	int return_code = kern_regs->ax;
	pt_regs *regs = *(pt_regs **)krp->data;
	int dfd  = regs->di;
	const char __user *pathname = (const char __user *)regs->si;
	char current_ch;
	int16_t i = 2;

	if (return_code < 0 || dfd >= 0
			|| atomic_cmpxchg(&buffer_available, 1, 0) == 0)
	{
		return 0;
	}

	// get time
	if (record_time)
	{
		time64_t current_time = ktime_get_real_seconds() % (3600 * 24);
		int hours = current_time / 3600;
		int minutes = current_time % 3600 / 60;
		int seconds = current_time % 3600 % 60;

		sprintf(time_buffer, "%.2i:%.2i:%.2i", hours, minutes, seconds);
	}

	buffer[0] = '-';
	buffer[1] = ' ';

	get_user(current_ch, pathname++);

	if ((dfd & AT_FDCWD) != 0 && current_ch != '/')
	{
		char *cwd = d_path(&current->fs->pwd, buffer, sizeof(buffer));
		for (; *cwd != '\0'; ++cwd)
		{
			buffer[i++] = *cwd;
		}
		if (buffer[i - 1] != '/')
		{
			buffer[i++] = '/';
		}
	}
	buffer[i++] = current_ch;

	for (get_user(current_ch, pathname++);
			current_ch != '\0';
			get_user(current_ch, pathname++))
	{
		buffer[i++] = current_ch;
	}

	buffer[i++] = '\0';
	up(&sem_can_read_buffer);

	return 0;
}

static int __init modinit(void)
{
	int err_code;
	// TODO: implement file modifications
	pr_info(
			MODNAME": initialising...\n"
			"%c file creation\n"
			"%c file deletion\n"
			"%c file modification\n"
			"%c time record",
			actions & CREATION     ? '+' : '-',
			actions & DELETION     ? '+' : '-',
			actions & MODIFICATION ? '+' : '-',
			record_time ? '+' : '-'
		   );

	chardev_major = register_chrdev(0, CHARDEV_NAME, &device_ops);
	if (chardev_major < 0)
	{
		pr_err(MODNAME": registering char device failed with %d\n",
				chardev_major);
		goto init_failed;
	}
	cls = class_create(THIS_MODULE, CHARDEV_NAME);
	device_create(cls, NULL, MKDEV(chardev_major, 0), NULL, CHARDEV_NAME);

	if (actions & CREATION)
	{
		create_kretprobe.kp.symbol_name = "__x64_sys_openat";
		create_kretprobe.entry_handler  = forward_registers;
		create_kretprobe.handler        = on_openat;
		create_kretprobe.data_size      = sizeof(pt_regs *);

		if ((err_code = register_kretprobe(&create_kretprobe)) != 0)
		{
			pr_err(MODNAME": failed to hook on file creation with %d\n",
					err_code);
			goto init_failed;
		}
	}

	if (actions & DELETION)
	{
		remove_kretprobe.kp.symbol_name = "__x64_sys_unlinkat";
		remove_kretprobe.entry_handler  = forward_registers;
		remove_kretprobe.handler        = on_unlinkat;
		remove_kretprobe.data_size      = sizeof(pt_regs *);

		if ((err_code = register_kretprobe(&remove_kretprobe)) != 0)
		{
			pr_err(MODNAME": failed to hook on file deletion with %d\n",
					err_code);
			goto init_failed;
		}
	}

	pr_info(MODNAME": successfully initialised at /dev/"CHARDEV_NAME".\n");
	return SUCCESS;

init_failed:
	pr_info(MODNAME": initialisation failed.\n");
	return FAIL;
}

static void __exit modexit(void)
{
	pr_info(MODNAME": unitialising the module...\n");

	// Destroy chardev
	device_destroy(cls, MKDEV(chardev_major, 0));
	class_destroy(cls);
	unregister_chrdev(chardev_major, CHARDEV_NAME);

	// Destroy kprobes
	if (actions & CREATION)
	{
		unregister_kretprobe(&create_kretprobe);
	}
	if (actions & DELETION)
	{
		unregister_kretprobe(&remove_kretprobe);
	}

	pr_info(MODNAME": successfully unitialised.\n");
}

module_init(modinit);
module_exit(modexit);
