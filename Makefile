MODULE_BASENAME := fcdlogger

obj-m += $(MODULE_BASENAME).o
$(MODULE_BASENAME)-y := main.o

KERN_DIR := /lib/modules/$(shell uname -r)/build
PWD := $(CURDIR)

build:
	make -C $(KERN_DIR) M=$(PWD) modules

install:
	make -C $(KERN_DIR) M=$(PWD) modules_install

clean:
	@# These are fixes to removing the compiledb
	@if [ -f compile_commands.json ]; then mv compile_commands.json cc.json.fuck; fi
	make -C $(KERN_DIR) M=$(PWD) clean
	@if [ -f cc.json.fuck ]; then mv cc.json.fuck compile_commands.json; fi

compiledb: build
	$(KERN_DIR)/scripts/clang-tools/gen_compile_commands.py
	@# This is fix to compiledb referring to cwd instead of kernel sources
	sed -i 's|\./|$(KERN_DIR)/|g' compile_commands.json

load:
	sudo insmod $(MODULE_BASENAME).ko

unload:
	sudo rmmod $(MODULE_BASENAME).ko

.PHONY: build clean compiledb load unload install
